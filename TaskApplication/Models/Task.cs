﻿using System.ComponentModel.DataAnnotations;
using TaskApplication.Areas.Identity.Data;

namespace TaskApplication.Models
{
    public class Task
    {
        [Key]
        public int Id { get; set; }

        public string Body { get; set; }
        public bool Finished { get; set; }
        public int UserId { get; set; }

    }
}
