﻿using Microsoft.EntityFrameworkCore;
using onboarding3.Models;

namespace web_api.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Article> Articles { get; set; }

    }
}
