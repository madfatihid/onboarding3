﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace onboarding3.Models
{
    public class Article
    {
        [Key]
        public int id { get; set; }

        [ForeignKey("User")]
        public int userid { get; set; }
        [ValidateNever]
        public virtual User user { get; set; }

        public string title { get; set; }

        [DataType(DataType.MultilineText)]
        public string body { get; set; }

        public DateTime created_at { get; set; } = DateTime.Now;
    }
}
