﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.IdentityModel.Tokens;
using onboarding3.Models;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace web_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;

        public UserController(DataContext dataContext, IConfiguration configuration, IMemoryCache memoryCache)
        {
            _context = dataContext;
            _configuration = configuration;
            _memoryCache = memoryCache;
        }

        [HttpGet]
        public async Task<ActionResult<List<User>>> Get()
        {
            return Ok(await _context.Users.ToListAsync());
        } 

        [HttpPost("Login")]
        public async Task<ActionResult<string>> Login(LoginDto user)
        {

            var logged = _memoryCache.Get<bool>("logged");
            if(logged)
            {
                return BadRequest("Already logged in");
            }

            var userCheck = await _context.Users.FirstOrDefaultAsync(u => u.email == user.email);
            if (userCheck == null)
            {
                return BadRequest("Email Not Found");
            }
            if (userCheck.password != user.password)
            {
                return BadRequest("Wrong Password");
            }

            var token = CreateToken(user.email);
            return Ok(token);

        }

        [HttpPost("Register")]
        public async Task<ActionResult<List<User>>> Register(RegisterDto user)
        {
            var logged = _memoryCache.Get<bool>("logged");
            if (logged)
            {
                return BadRequest("Already logged in");
            }
            if (_context.Users.Any(u => u.email == user.email))
            {
                return BadRequest("Email already exist");
            }
            if (_context.Users.Any(u => u.username == user.username))
            {
                return BadRequest("Username already exist");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest("Model not valid");
            }
            var userNew = new User() { email = user.email, password = user.password, username = user.username };
            userNew.created_at = DateTime.Now;
            _context.Add(userNew);
            await _context.SaveChangesAsync();

            var token = CreateToken(user.email);
            return Ok(token);
        }

        private string CreateToken(string email)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Email, email)
            };

            var key = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(_configuration.GetSection("AppSettings:Token").Value));

            var cred = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: cred
            );

            var jwt = new JwtSecurityTokenHandler().WriteToken(token);

            _memoryCache.Set("logged", true, TimeSpan.FromMinutes(1));

            return jwt;
        }

        [HttpPost("Logout")]
        public async Task<ActionResult<string>> Logout()
        {
            _memoryCache.Set("logged", false);
            return Ok();
        }

    }
}
