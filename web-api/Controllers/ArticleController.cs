﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using onboarding3.Models;

namespace web_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ArticleController : ControllerBase
    {
        private readonly DataContext _context;

        public ArticleController(DataContext dataContext)
        {
            _context = dataContext;
        }

        [HttpGet]
        public async Task<ActionResult<List<Article>>> Get()
        {
            return Ok(await _context.Articles.ToListAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Article>> Get(int id)
        {
            var article = await _context.Articles.FirstOrDefaultAsync(a => a.id == id);
            if (article == null)
                return BadRequest("Article not found");
            return Ok(article);
        }

        [HttpPost, Authorize]
        public async Task<ActionResult<List<Article>>> Post(Article article)
        {
            _context.Articles.Add(article);
            await _context.SaveChangesAsync();

            return Ok(article);
        }
        [HttpPut]
        public async Task<ActionResult<List<Article>>> Put(Article article)
        {
            var dbArticle = await _context.Articles.FirstOrDefaultAsync(a => a.id == article.id);
            if (dbArticle == null)
                return BadRequest("Article not found");

            dbArticle = article;

            await _context.SaveChangesAsync();

            return Ok(article);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<List<Article>>> Delete(int id)
        {
            var dbArticle = await _context.Articles.FirstOrDefaultAsync(a => a.id == id);
            if (dbArticle == null)
                return BadRequest("Article not found");

            _context.Articles.Remove(dbArticle);
            await _context.SaveChangesAsync();

            return Ok(dbArticle);
        }
    }
}
