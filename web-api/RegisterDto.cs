﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace web_api
{
    public class RegisterDto
    {
        [DataType(DataType.EmailAddress)]
        public string email { get; set; }

        [DataType(DataType.Password)]
        [MinLength(6)]
        public string password { get; set; }

        [Column(TypeName = "nvarchar(32)")]
        public string username { get; set; }

    }
}
