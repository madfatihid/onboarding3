﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace onboarding3.Models
{
    public class User
    {
        [Key]
        public int id { get; set; }

        [DataType(DataType.EmailAddress)]
        public string email { get; set; }

        [DataType(DataType.Password)]
        [MinLength(6)]
        public string password { get; set; }

        [Column(TypeName = "nvarchar(32)")]
        public string username { get; set; }
        
        public virtual ICollection<Article> articles { get; set; }

        public DateTime created_at { get; set; } = DateTime.Now;
    }
}
