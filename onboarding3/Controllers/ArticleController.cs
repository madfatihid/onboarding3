﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using onboarding3.Models;

namespace onboarding3.Controllers
{
    public class ArticleController : Controller
    {
        private readonly DataContext _context;

        public ArticleController(DataContext context)
        {
            _context = context;
        }

        // GET: Article
        /*public async Task<IActionResult> Index()
        {
            var dataContext = _context.Articles.Include(a => a.user);
            return View(await dataContext.ToListAsync());
        }*/

        // GET: Article
        public async Task<IActionResult> Index(int? id)
        {
            if (id == null || _context.Articles == null)
            {
                var dataContext = _context.Articles.Include(a => a.user);
                return View(await dataContext.ToListAsync());
            }

            var article = await _context.Articles
                .Include(a => a.user)
                .FirstOrDefaultAsync(m => m.id == id);
            if (article == null)
            {
                return NotFound();
            }

            return View("Details", article);
        }

        // GET: Article/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Articles == null)
            {
                return NotFound();
            }

            var article = await _context.Articles
                .Include(a => a.user)
                .FirstOrDefaultAsync(m => m.id == id);
            if (article == null)
            {
                return NotFound();
            }

            return View(article);
        }

        // GET: Article/Create
        public IActionResult Create()
        {
            var sessionId = HttpContext.Session.GetInt32("id");
            if (sessionId == null)
            {
                return RedirectToAction("Login", "User");
            }

            ViewData["userid"] = new SelectList(_context.Users, "id", "id");
            return View();
        }

        // POST: Article/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,userid,title,body,created_at")] Article article)
        {
            if (ModelState.IsValid)
            {
                article.userid = (int)HttpContext.Session.GetInt32("id");
                article.created_at = DateTime.Now;
                _context.Add(article);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["userid"] = new SelectList(_context.Users, "id", "id", article.userid);
            return View(article);
        }

        // GET: Article/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Articles == null)
            {
                return NotFound();
            }

            var article = await _context.Articles.FindAsync(id);
            if (article == null)
            {
                return NotFound();
            }
            ViewData["userid"] = new SelectList(_context.Users, "id", "id", article.userid);
            return View(article);
        }

        // POST: Article/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("id,userid,title,body,created_at")] Article article)
        {
            if (id != article.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var oldArticle = _context.Articles.FirstOrDefault(item => item.id == article.id);

                // Validate entity is not null
                if (oldArticle != null)
                {
                    // Answer for question #2

                    // Make changes on entity
                    oldArticle.title = article.title;
                    oldArticle.body = article.body;

                    /* If the entry is being tracked, then invoking update API is not needed. 
                      The API only needs to be invoked if the entry was not tracked. 
                      https://www.learnentityframeworkcore.com/dbcontext/modifying-data */
                    // context.Products.Update(entity);

                    // Save changes in database
                    _context.SaveChanges();
                }
                /*
                try
                {
                    article.userid = (int)HttpContext.Session.GetInt32("id");
                    article.created_at = DateTime.Now;
                    _context.Update(article);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ArticleExists(article.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }*/
                return RedirectToAction(nameof(Index));
            }
            ViewData["userid"] = new SelectList(_context.Users, "id", "id", article.userid);
            return View(article);
        }

        // GET: Article/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Articles == null)
            {
                return NotFound();
            }

            var article = await _context.Articles
                .Include(a => a.user)
                .FirstOrDefaultAsync(m => m.id == id);
            if (article == null)
            {
                return NotFound();
            }

            return View(article);
        }

        // POST: Article/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Articles == null)
            {
                return Problem("Entity set 'DataContext.Articles'  is null.");
            }
            var article = await _context.Articles.FindAsync(id);
            if (article != null)
            {
                _context.Articles.Remove(article);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ArticleExists(int id)
        {
          return (_context.Articles?.Any(e => e.id == id)).GetValueOrDefault();
        }
    }
}
