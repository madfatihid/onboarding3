﻿
DROP TABLE IF EXISTS "articles";
DROP TABLE IF EXISTS "users";

CREATE TABLE "users" (
    "id" BIGINT NOT NULL PRIMARY KEY IDENTITY,
    "email" VARCHAR (256) NOT NULL UNIQUE,
    "password" TEXT NOT NULL,
    "username" TEXT NOT NULL,
    "created_at" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "articles" (
    "id" BIGINT NOT NULL PRIMARY KEY IDENTITY,
    "user_id" BIGINT NOT NULL,
    "title" VARCHAR (100) NOT NULL,
    "body" TEXT NOT NULL,
    "created_at" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);

ALTER TABLE
    "articles" ADD CONSTRAINT "articles_users_id_foreign" FOREIGN KEY("user_id") REFERENCES "users"("id");
